//
//  NotrosSDK.h
//  NotrosSDK
//
//  Created by Jacob Wilschrey on 9/4/19.
//  Copyright © 2019 Elun. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for NotrosSDK.
FOUNDATION_EXPORT double NotrosSDKVersionNumber;

//! Project version string for NotrosSDK.
FOUNDATION_EXPORT const unsigned char NotrosSDKVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <NotrosSDK/PublicHeader.h>


