Pod::Spec.new do |s|  
    s.name                  = 'NotrosSDK'
    s.version               = '0.1.10'
    s.summary               = 'SDK oficial de integración de experiencias contextuales para Swift/iOS'
    s.homepage              = 'http://getnotros.com'

    s.author                = { 'Jacob Wilschrey' => 'jacob@elun.cl' }
    s.license               = { :type => 'MIT License', :file => 'LICENSE' }

    # s.source                = { :git => '.', :tag => "#{s.version}" }
    s.source                = { :git => 'https://bitbucket.org/elun_cl/notrossdk-ios-smu.git', :tag => "#{s.version}" }

    s.platform              = :ios
    s.ios.deployment_target = '10.0'

    s.ios.vendored_frameworks = 'NotrosSDK.framework'
    s.frameworks 	        = 'NotrosSDK'

    s.dependency "Alamofire", "~> 4.8"
    s.dependency "ObjectMapper", "~> 3.5"

    s.swift_version = "4.2"    
end
